import React from 'react';
import './App.scss';
import { Container, Row, Col } from 'react-bootstrap';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import DataEntry from './components/DataEntry';
import Article from './components/Article';
import FAQPage from './pages/FAQs';

const App = () => (
    <Router>
        <Container>
            <Row>
                <Col>
                    <div>
                        <Route path="/" exact component={FAQPage} />
                        <Route path="/i-want-to" component={FAQPage} />
                        <Route path="/entry" render={() => <DataEntry />} />
                        <Route path="/article" render={() => <Article />} />
                    </div>
                </Col>
            </Row>
        </Container>
    </Router>
);

export default App;
