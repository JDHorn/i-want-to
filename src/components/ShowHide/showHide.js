import React, { useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './showHide.scss';
import Octicon, {
    ChevronDown,
    ChevronRight,
} from '@githubprimer/octicons-react';

const ShowHide = ({ show, showTitle, hideTitle, children }) => {
    const [display, toggle] = useState(show);
    return (
        <span className="show-hide">
            <span
                className={classnames('toggle', { active: display })}
                onClick={() => toggle(!display)}>
                {display ? (
                    <Octicon icon={ChevronDown} verticalAlign="middle" />
                ) : (
                    <Octicon icon={ChevronRight} verticalAlign="middle" />
                )}
                {display ? hideTitle : showTitle}
            </span>
            <CSSTransition
                in={display}
                timeout={250}
                classNames="show-hide-animated">
                <span style={{ display: 'inline-block' }}>
                    {display ? children : null}
                </span>
            </CSSTransition>
        </span>
    );
};

ShowHide.defaultProps = {
    show: false,
    showTitle: 'Show script',
    hideTitle: 'Hide script',
};

ShowHide.propTypes = {
    show: PropTypes.bool,
    showTitle: PropTypes.string,
    hideTitle: PropTypes.string,
};

export default ShowHide;
