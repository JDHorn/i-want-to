import React, { Component, useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import './faqs.scss';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import Octicon, {
    ChevronDown,
    ChevronRight,
} from '@githubprimer/octicons-react';

import classnames from 'classnames';

class FAQs extends Component {
    constructor(props) {
        super(props);

        let activeSection =
            props.activeSection || this.props.data.sections[0].section;

        this.state = {
            activeSection,
            filteredData: this.filterData(activeSection),
        };

        // this.sectionClick = this.sectionClick.bind(this);
    }

    filterData = section => {
        return this.props.data.articles.filter(
            article => article.section === section
        );
    };

    sectionClick = section => {
        this.setState({
            activeSection: section,
            filteredData: this.filterData(section),
        });
    };

    render = () => (
        <div id="faq-component" className="adg-landing-faqs">
            <Row>
                <Col sm={3}>
                    <Sections
                        sections={this.props.data.sections}
                        sectionClickCallback={this.sectionClick}
                        activeSection={this.state.activeSection}
                    />
                </Col>
                <Col sm={9}>
                    <Articles articles={this.state.filteredData} />
                </Col>
            </Row>
        </div>
    );
}

FAQs.propTypes = {
    data: PropTypes.object,
};

FAQs.defaultProps = {
    data: { articles: [], sections: [] },
};

const Sections = ({ sections, activeSection, sectionClickCallback }) => (
    <ul className="adg-landing-faqs__menu">
        {sections.map((menuItem, index) => (
            <li
                key={index}
                className={classnames('adg-landing-faqs__menu__item', {
                    'adg-landing-faqs__menu__item--active':
                        activeSection === menuItem.section,
                })}>
                {activeSection === menuItem.section ? (
                    <span className="adg-landing-faqs__menu__link">
                        {menuItem.friendlyName}
                    </span>
                ) : (
                    <button
                        onClick={() => sectionClickCallback(menuItem.section)}
                        className="adg-landing-faqs__menu__link">
                        {menuItem.friendlyName}
                    </button>
                )}
            </li>
        ))}
    </ul>
);

export const Articles = ({ articles }) => (
    <div
        id="faq-articles"
        className="adg-landing-faqs__content adg-landing-faqs__content--active">
        {articles.length ? (
            articles.map(article => (
                <Article
                    key={article.question().props.children}
                    article={article}
                />
            ))
        ) : (
            <h4>No articles to display</h4>
        )}
    </div>
);

const Article = ({ article: { question, answer, seldom }, active = false }) => {
    const [isActive, toggle] = useState(active);
    return (
        <CSSTransition
            in={isActive}
            timeout={250}
            classNames="article-animated">
            <article
                className={classnames('adg-landing-faq-item', {
                    'adg-landing-faq-item--active': isActive,
                    seldom,
                })}>
                <div className="adg-landing-faq-item__q">
                    <section
                        onClick={() => {
                            toggle(!isActive);
                        }}>
                        {isActive ? (
                            <Octicon
                                icon={ChevronDown}
                                verticalAlign="middle"
                            />
                        ) : (
                            <Octicon
                                icon={ChevronRight}
                                verticalAlign="middle"
                            />
                        )}{' '}
                        ... {question()}
                    </section>

                    {isActive && (
                        <div className="adg-landing-faq-item__a">
                            <section>{answer()}</section>
                        </div>
                    )}
                </div>
            </article>
        </CSSTransition>
    );
};

export default FAQs;
