import React from 'react';

export const ExternalLink = ({
    children,
    text = children,
    title = 'Opens in a new window',
    target = '_blank',
}) => (
    <a href={children} rel="noopener noreferrer" target={target} title={title}>
        {text}
    </a>
);
