import React, { Component } from 'react';
import sections from '../../data/sections';
import MonacoEditor from 'react-monaco-editor';
import { Row, Col } from 'react-bootstrap';

const initialState = {
    section: '',
    seldom: false,
    question: '',
    answer: `<p></p>\n<CodeCopy></CodeCopy>`,
    copySuccess: false,
};

class Article extends Component {
    state = initialState;

    successDuration = 1000;

    reset = () => {
        this.setState(() => initialState);
    };

    editorDidMount = (editor, monaco) => {
        const model = editor.getModel();
        monaco.editor.setModelLanguage(model, 'html');
    };

    copy = () => {
        let range = document.createRange();
        range.selectNode(this.el);
        const sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        document.execCommand('copy');
        sel.removeAllRanges();
        this.setState({ copySuccess: true });

        setTimeout(() => {
            this.setState({ copySuccess: false });
        }, this.successDuration);
    };

    handleInputChange = event => {
        const target = event.target;
        const value =
            target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value,
        });
    };

    onMonacoChange = (newValue, e) => {
        this.setState({ answer: newValue });
    };

    render() {
        const options = {
            selectOnLineNumbers: true,
        };
        const { section, seldom, question, answer, copySuccess } = this.state;
        return (
            <div>
                <Row>
                    <Col md={6}>
                        <label>
                            Section:
                            <br />
                            <select
                                name="section"
                                value={section}
                                onChange={this.handleInputChange}>
                                <option value="">Choose a section...</option>
                                {sections.map(({ section, friendlyName }) => (
                                    <option key={section} value={section}>
                                        {friendlyName}
                                    </option>
                                ))}
                            </select>
                        </label>
                        <br />
                        <label>
                            Seldom:{' '}
                            <input
                                type="checkbox"
                                name="seldom"
                                checked={seldom}
                                value={seldom}
                                onChange={this.handleInputChange}
                            />
                        </label>
                        <br />
                        <label>
                            I want to...
                            <br />
                            <input
                                value={question}
                                name="question"
                                onChange={this.handleInputChange}
                            />
                        </label>
                        <br />
                        <label>
                            Solution:
                            <br />
                            <MonacoEditor
                                width="400"
                                height="200"
                                language="html"
                                value={answer}
                                options={options}
                                onChange={this.onMonacoChange}
                                editorDidMount={this.editorDidMount}
                            />
                        </label>
                    </Col>
                    <Col md={6}>
                        <code
                            ref={el => (this.el = el)}
                            style={{ whiteSpace: 'pre' }}>{`{
    section: '${section || 'pick one'}',
    seldom: ${seldom},
    question: () => <span>${question || ''}</span>,
    answer: () => (
        <span>
            ${answer || ''}
        </span>
    ),
},`}</code>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-center">
                        <button onClick={this.copy}>
                            {copySuccess ? (
                                <span style={{ color: 'orangered' }}>
                                    Copied!
                                </span>
                            ) : (
                                <span>Copy code</span>
                            )}
                        </button>{' '}
                        <button onClick={this.reset}>Reset</button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Article;
