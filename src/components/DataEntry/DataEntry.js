import React, { Component } from 'react';
import sections from '../../data/sections';
import contentSections from '../../data/articles';
window.contentSections = contentSections;

class DataEntry extends Component {
    state = { section: undefined };

    setSection = e => {
        this.setState({ section: e.target.value });
    };

    render() {
        const { section } = this.state;
        const data = section
            ? contentSections.filter(article => article.section === section)
            : undefined;
        return (
            <div>
                <select onChange={this.setSection}>
                    <option value="">Choose a section</option>
                    {sections.map(section => (
                        <option key={section.section} value={section.section}>
                            {section.friendlyName}
                        </option>
                    ))}
                </select>
                {section && (
                    <code style={{ whiteSpace: 'pre' }}>
                        {data.map(
                            ({ section, seldom, question, answer }) =>
                                `
{
    section: '${section}',
    seldom: ${seldom ? seldom.toString() : 'false'}
    question:  ${question().props.children},
    answer:  (
        ${
            Array.isArray(answer().props.children)
                ? answer().props.children.map(el => el.props.children)
                : answer().props.children.props.children
        }
    ),
},`
                        )}
                    </code>
                )}
            </div>
        );
    }
}

export default DataEntry;
