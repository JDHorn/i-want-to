import React, { Component } from 'react';
import './code-copy.scss';

export default class CodeCopy extends Component {
    state = { copySuccess: false };
    successDuration = 750;

    copy = () => {
        let range = document.createRange();
        range.selectNode(this.el);
        const sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        document.execCommand('copy');
        sel.removeAllRanges();
        this.setState({ copySuccess: true });

        setTimeout(() => {
            this.setState({ copySuccess: false });
        }, this.successDuration);
    };

    render() {
        const { children, successText = 'Copied!', inline } = this.props;
        return (
            <span className="code-copy">
                <code
                    ref={el => {
                        this.el = el;
                    }}
                    title="click to copy"
                    onClick={this.copy}
                    className={inline ? 'inline' : ''}>
                    {children}
                    {this.state.copySuccess && (
                        <span className="message">{successText}</span>
                    )}
                </code>
            </span>
        );
    }
}
