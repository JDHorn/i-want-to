import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import FAQs from '../components/FAQs';
import data from '../data';

class FAQPage extends Component {
    render() {
        return (
            <Container role="main" aria-labelledby="main-heading">
                <Row>
                    <Col sm={8} md={6}>
                        <h1 id="main-heading">I want to ...</h1>
                        <p>
                            While these tips are quite mac and a.com-centric,
                            the commands for Windows and other projects in
                            general will often be very similar.
                        </p>
                        <p>Greyed out tips are used less often.</p>
                        <p>
                            <em>
                                Choose a section to view tips, then click the
                                provided code to copy it
                            </em>
                        </p>
                        <p>
                            Care to contribute to this content? Open your pull
                            requests against{' '}
                            <a href="https://bitbucket.org/JDHorn/i-want-to/branches/">
                                this repo
                            </a>
                            .
                        </p>
                    </Col>
                </Row>
                <FAQs data={data} />
            </Container>
        );
    }
}

export default FAQPage;
