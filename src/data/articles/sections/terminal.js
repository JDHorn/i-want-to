import React from 'react';
import CodeCopy from '../../../components/codeCopy';
import { sortArticles } from '../../../components/utilities';
export default sortArticles(
    [
        {
            section: 'terminal',
            question: () => <span>clean up node modules</span>,
            answer: () => (
                <>
                    <h2>Cleaning up node modules</h2>
                    <p>
                        Let's face it, as developers, over time our node modules
                        tent to accumulate. Old projects we don't use often,
                        personal projects, POCs, etc. - individually their{' '}
                        <code>node_modules</code> folders can eat up literally
                        hundreds of megabytes of disc space, and collectively
                        they can eat up gigabytes.
                    </p>
                    <p>
                        Going through each individual folder and manually
                        deleting <code>node_modules</code> folders can be
                        time-consuming - so what's a good way to automate the
                        process?
                    </p>

                    <h2>Let's get going</h2>
                    <p>
                        First, be sure you're in the proper directory; in my
                        case this is my <code>source-code</code> folder:
                        <CodeCopy inline>cd ~/source-code</CodeCopy>
                    </p>
                    <h3>Calculate disc usage</h3>
                    <p>How much space is this actually using?</p>
                    <CodeCopy>
                        find . -name "node_modules" -type d -prune | xargs du
                        -chs
                    </CodeCopy>
                    <h3>Do the delete</h3>
                    <p>Let's do it</p>
                    <CodeCopy>
                        find . -name "node_modules" -type d -prune -exec rm -rf
                        '{}' +
                    </CodeCopy>
                </>
            ),
        },
        {
            section: 'terminal',
            question: () => <span>ssh - ensure ssh-agent is enabled</span>,
            answer: () => (
                <>
                    <CodeCopy>eval `ssh-agent`</CodeCopy>
                    <p>
                        More info available on{' '}
                        <a href="https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html">
                            Bitbucket
                        </a>
                    </p>
                </>
            ),
        },
        {
            section: 'terminal',
            question: () => <span>ssh - generate an ssh key</span>,
            answer: () => (
                <>
                    <CodeCopy>cd ~/.ssh</CodeCopy>
                    <CodeCopy>
                        ssh-keygen -t rsa -C "&lt;your_email@example.com&gt;"
                    </CodeCopy>
                </>
            ),
        },
        {
            section: 'terminal',
            question: () => <span>base64 encode a string</span>,
            answer: () => (
                <>
                    <CodeCopy>
                        openssl base64 -e &lt;&lt;&lt; "this is a string"
                    </CodeCopy>
                    <p>
                        Returns <code>dGhpcyBpcyBhIHN0cmluZwo=</code>
                    </p>
                </>
            ),
        },
        {
            section: 'terminal',
            question: () => <span>decode a base64 string</span>,
            answer: () => (
                <>
                    <CodeCopy>
                        openssl base64 -d &lt;&lt;&lt; dGhpcyBpcyBhIHN0cmluZwo=
                    </CodeCopy>
                    <p>
                        Returns <code>this is a string</code>
                    </p>
                </>
            ),
        },
        {
            section: 'terminal',
            question: () => <span>ssh - add an SSH key</span>,
            answer: () => (
                <>
                    <p>Ensure ssh-agent is enabled, then ...</p>
                    <CodeCopy>ssh-add ~/.ssh/&lt;key-name&gt;</CodeCopy>
                </>
            ),
        },
        {
            section: 'terminal',
            question: () => <span>ssh - copy an SSH key to the clipboard</span>,
            answer: () => (
                <CodeCopy>cat ~/.ssh/&lt;key-name&gt;.pub | pbcopy .</CodeCopy>
            ),
        },
        {
            section: 'terminal',
            question: () => (
                <span>
                    start a simple HTTP server in any folder on your mac
                </span>
            ),
            answer: () => (
                <>
                    <code>cd</code> into any folder and ...
                    <CodeCopy>python -m SimpleHTTPServer 8000</CodeCopy>
                </>
            ),
        },
        {
            section: 'terminal',
            question: () => (
                <span>switch back to the branch you were just on</span>
            ),
            answer: () => (
                <>
                    <p>For example:</p>
                    <pre>
                        <code>
                            <span className="terminal">
                                git checkout
                                long_branch_name_you_can_never_remember
                            </span>
                            <br />
                            Switched to branch
                            'long_branch_name_you_can_never_remember'
                            <br />
                            <br />
                            <span className="terminal">
                                git checkout master
                            </span>
                            <br />
                            Switched to branch 'master'
                            <br />
                            <br />
                            <strong>
                                <CodeCopy inline>
                                    <span className="terminal">
                                        git checkout -
                                    </span>
                                </CodeCopy>
                            </strong>
                            <br />
                            Switched to branch
                            'long_branch_name_you_can_never_remember'
                        </code>
                    </pre>
                    <p>
                        This is actually shorthand for{' '}
                        <CodeCopy inline>git checkout @{-1}</CodeCopy>
                    </p>
                </>
            ),
        },
    ],
    'question'
);
