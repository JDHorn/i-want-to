import React from 'react';
import { sortArticles } from '../../../components/utilities';
import { ExternalLink } from '../../../components/Links';
import CodeCopy from '../../../components/codeCopy';
export default sortArticles([
    {
        section: 'gutenberg',
        question: () => (
            <span>
                use insertBlocks to insert into nested block programmatically
            </span>
        ),
        answer: () => (
            <>
                <p>See these URLs:</p>
                <ul>
                    <li>
                        <ExternalLink>
                            https://github.com/WordPress/gutenberg/issues/10296
                        </ExternalLink>
                    </li>
                    <li>
                        <ExternalLink>
                            https://github.com/TomodomoCo/gutenberg-block-columns/blob/master/src/block/inspector.js
                        </ExternalLink>
                    </li>
                </ul>
            </>
        ),
    },
    {
        section: 'gutenberg',
        question: () => <span>get props, etc. of InnerBlocks</span>,
        answer: () => (
            <>
                <p>
                    See this URL:{' '}
                    <ExternalLink>
                        https://github.com/WordPress/gutenberg/issues/10479
                    </ExternalLink>
                </p>
                <CodeCopy>
                    {'const { clientId } = this.props;'}
                    <br />
                    {'const { select } = wp.data;'}
                    <br />
                    <br />
                    {
                        "const parentBlock = select( 'core/editor' ).getBlocksByClientId( clientId )[ 0 ];"
                    }
                    <br />
                    {'const childBlocks = parentBlock.innerBlocks;'}
                </CodeCopy>
            </>
        ),
    },
    {
        section: 'gutenberg',
        question: () => <span>view the block attributes glossary</span>,
        answer: () => (
            <>
                <p>
                    Attributes are the options and content that control blocks.
                    The block development process involves defining how these
                    attributes are drawn from the markup that is saved in the
                    database.
                </p>

                <ExternalLink>
                    https://design.oit.ncsu.edu/docs/gutenberg/block-attributes/
                </ExternalLink>
            </>
        ),
    },
]);
