import React from 'react';
import { sortArticles } from '../../../components/utilities';
import CodeCopy from '../../../components/codeCopy';
export default sortArticles([
    {
        section: 'misc',
        question: () => <span>optimize images</span>,
        answer: () => (
            <>
                <CodeCopy>yarn global add imagemin</CodeCopy>
                <CodeCopy>imagemin &lt;dir> * -o &lt;dir></CodeCopy>
            </>
        ),
    },
    {
        section: 'misc',
        question: () => <span>Emmet shortcuts</span>,
        answer: () => (
            <>
                <p>
                    What is Emmet? Emmet is a quick way to code HTML, hit Tab,
                    and have it auto-expand. For example:{' '}
                    <CodeCopy inline>ul.generic-list>lorem5.item*4</CodeCopy>{' '}
                    would expand into:
                </p>
                <pre>
                    <code>
                        &lt;ul className="generic-list">
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&lt;li className="item">Lorem
                        ipsum dolor sit amet.&lt;/li>
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&lt;li className="item">Nulla
                        corporis nihil vitae molestias?&lt;/li>
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&lt;li
                        className="item">Obcaecati mollitia doloribus quas
                        quibusdam.&lt;/li>
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&lt;li className="item">Enim
                        impedit ratione distinctio quia!&lt;/li>
                        <br />
                        &lt;/ul>
                    </code>
                </pre>
                <p>
                    <a href="https://docs.emmet.io/">Emmet documentation</a>
                </p>
            </>
        ),
    },
    {
        section: 'misc',
        question: () => <span>create a new React app</span>,
        answer: () => (
            <>
                <p>
                    Replace <code>&lt;folder-name></code> with the name of
                    whatever folder you'd like to create your app in.
                </p>
                <CodeCopy>npx create-react-app &lt;folder-name></CodeCopy>
            </>
        ),
    },
]);
