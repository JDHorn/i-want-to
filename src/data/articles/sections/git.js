import CodeCopy from '../../../components/codeCopy';
import React from 'react';
import { sortArticles } from '../../../components/utilities';
import { ExternalLink } from '../../../components/Links';

export default sortArticles([
    {
        section: 'git',
        question: () => <span>grab all changes from the remote</span>,
        answer: () => (
            <>
                <p>
                    This command `fetches` all changes, but does not apply them.
                </p>
                <CodeCopy>git fetch</CodeCopy>
            </>
        ),
    },
    {
        section: 'git',
        seldom: false,
        question: () => (
            <span>update my current branch with changes from the remote</span>
        ),
        answer: () => (
            <>
                <p>Works when your branch can be fast-forwarded</p>
                <CodeCopy>git pull</CodeCopy>
            </>
        ),
    },
    {
        section: 'git',
        seldom: true,
        question: () => <span>show the url of the remote</span>,
        answer: () => <CodeCopy>git config --get remote.origin.url</CodeCopy>,
    },
    {
        section: 'git',
        seldom: true,
        question: () => (
            <span>forcefully update my current branch to match the remote</span>
        ),
        answer: () => (
            <>
                <p>
                    This set of commands grabs all changes from origin, then in
                    detached head mode, moves the local branch's commit pointer
                    to the same position as that of the remote. This is more or
                    less a hard reset of your local branch
                </p>
                <CodeCopy>git fetch</CodeCopy>
                <CodeCopy>git checkout origin/&lt;branch></CodeCopy>
                <CodeCopy>git branch -f &lt;branch></CodeCopy>
                <CodeCopy>git checkout &lt;branch></CodeCopy>
            </>
        ),
    },
    {
        section: 'git',
        question: () => <span>sort git branches by date</span>,
        answer: () => (
            <>
                <CodeCopy>
                    git for-each-ref --sort=-committerdate refs/heads/
                </CodeCopy>
            </>
        ),
    },
    {
        section: 'git',
        question: () => (
            <span>list the last 10 branches you've had checked out</span>
        ),
        answer: () => (
            <>
                <CodeCopy>
                    git for-each-ref --sort=-committerdate --count=10
                    --format="%(refname)" refs/heads/
                </CodeCopy>
            </>
        ),
    },
    {
        section: 'git',
        question: () => (
            <span>git to remember how I've resolved past conflicts!</span>
        ),
        answer: () => (
            <>
                <p>
                    Completing a rebase where you need to resolve the same
                    resolution repeatedly is a painful process. Thankfully,{' '}
                    <code>git</code> has a workaround called "
                    <strong>RE</strong>member <strong>RE</strong>
                    corded <strong>RE</strong>solution" or <code>rerere</code>
                </p>
                <p>
                    To enable this feature, use this command:{' '}
                    <CodeCopy inline>
                        git config --global rerere.enabled 1
                    </CodeCopy>
                </p>
                <p>
                    Then run <CodeCopy inline>git rerere</CodeCopy> in your repo
                </p>

                <p>
                    See the complete feature documentation here:{' '}
                    <ExternalLink>
                        https://git-scm.com/docs/git-rerere
                    </ExternalLink>
                </p>
            </>
        ),
    },
    {
        section: 'git',
        question: () => <span>cut a release</span>,
        answer: () => (
            <ol>
                <li>
                    <CodeCopy inline>git checkout develop</CodeCopy>
                </li>
                <li>
                    <CodeCopy inline>git pull</CodeCopy> to grab the latest from
                    origin
                </li>
                <li>
                    <CodeCopy inline>git checkout master</CodeCopy>
                </li>
                <li>
                    <CodeCopy inline>git pull</CodeCopy> to grab the latest from
                    origin
                </li>
                <li>
                    <CodeCopy inline>git merge develop</CodeCopy>
                </li>
                <li>
                    <CodeCopy inline>npm version minor</CodeCopy> updates the
                    package.json file and creates a tag. <br />
                    Depending on the changes, <code>minor</code> could also be{' '}
                    <code>major</code> or <code>patch</code>.
                </li>
                <li>
                    <CodeCopy inline>git push && git push --tags</CodeCopy>
                    <br />
                    Note that once master is pushed to origin, there is no going
                    back. This is because history rewriting on origin is
                    disabled for master. To "undo" changes to master requires a
                    revert commit.
                </li>
                <li>
                    <CodeCopy inline>git checkout develop</CodeCopy>
                </li>
                <li>
                    <CodeCopy inline>git rebase master</CodeCopy>
                </li>
                <li>
                    <CodeCopy inline>git push</CodeCopy>
                </li>
            </ol>
        ),
    },
    {
        section: 'git',
        question: () => (
            <span>
                clean my local repo and file structure of untracked files
            </span>
        ),
        answer: () => (
            <>
                <p>
                    Sometimes it's useful to purge untracked files and
                    directories from your git repo.
                </p>
                <dl>
                    <dt>Clean files and directories</dt>
                    <dd>
                        <CodeCopy>git clean -d</CodeCopy>
                    </dd>
                    <dt>Interactive mode</dt>
                    <dd>
                        <CodeCopy>git clean -di</CodeCopy>
                    </dd>
                    <dt>Dry run</dt>
                    <dd>
                        <CodeCopy>git clean -dn</CodeCopy>
                    </dd>
                </dl>
                <p>
                    More info on the command can be found here:{' '}
                    <a href="https://git-scm.com/docs/git-clean">
                        <code>git clean</code>
                    </a>
                </p>
            </>
        ),
    },
]);
