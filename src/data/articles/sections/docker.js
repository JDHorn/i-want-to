import CodeCopy from '../../../components/codeCopy';
import React from 'react';
import { sortArticles } from '../../../components/utilities';

export default sortArticles([
    {
        section: 'docker',
        question: () => <span>run Asurion.com locally</span>,
        answer: () => (
            <>
                <p>Starts the site in background mode</p>
                <CodeCopy>yarn local-start</CodeCopy>
            </>
        ),
    },
    {
        section: 'docker',
        question: () => <span>stop Docker</span>,
        answer: () => (
            <>
                <p>Stop Docker and remove all containers</p>
                <CodeCopy>yarn local-stop</CodeCopy>
            </>
        ),
    },
    {
        section: 'docker',
        seldom: true,
        question: () => <span>install the wp cli</span>,
        answer: () => (
            <>
                <p>Step 1 in preparing to apply version press updates</p>
                <CodeCopy>
                    cd /var/www/html && ~/bin/docker/install-wp-cli
                </CodeCopy>
            </>
        ),
    },
    {
        section: 'docker',
        question: () => <span>2) apply version press changes</span>,
        answer: () => (
            <>
                <p>
                    After pulling from the repo or checking out a branch, run
                    this command
                </p>
                <CodeCopy>
                    cd /var/www/html && wp --allow-root vp apply-changes
                </CodeCopy>
                <p>Or individually ...</p>
                <CodeCopy>cd /var/www/html</CodeCopy>
                <CodeCopy>wp --allow-root vp apply-changes</CodeCopy>
            </>
        ),
    },
    {
        section: 'docker',
        question: () => <span>rebuild the container(s)</span>,
        seldom: true,
        answer: () => (
            <>
                <p>Used when there are changes to the dockerFile</p>
                <CodeCopy>yarn local-build</CodeCopy>
            </>
        ),
    },
    {
        section: 'docker',
        question: () => (
            <span>start the site and keep the process attached</span>
        ),
        answer: () => (
            <>
                <p>
                    Takes control of the terminal process in which it's
                    launched.
                </p>
                <p>Can be stopped with ⌃c</p>
                <CodeCopy>yarn local-run</CodeCopy>
            </>
        ),
    },
    {
        section: 'docker',
        question: () => <span>list running containers</span>,
        answer: () => (
            <>
                <p>
                    Lists all containers which are currently running on your
                    system
                </p>
                <CodeCopy>docker ps</CodeCopy>
            </>
        ),
    },
    {
        section: 'docker',
        question: () => (
            <span>1) log into the terminal on the wordpress instance</span>
        ),
        answer: () => (
            <CodeCopy>
                docker exec -i -t asuriondotcom_wordpress_1 bash
            </CodeCopy>
        ),
    },
    {
        section: 'docker',
        seldom: true,
        question: () => <span>install vim in a docker container</span>,
        answer: () => <CodeCopy>yum install -y vim</CodeCopy>,
    },
    {
        section: 'docker',
        seldom: true,
        question: () => <span>use yum in a docker container</span>,
        answer: () => (
            <>
                <p>
                    The yum command is the primary tool for getting, installing,
                    deleting, querying, and otherwise managing Red Hat
                    Enterprise Linux RPM software packages from official Red Hat
                    software repositories, as well as other third-party
                    repositories.
                </p>
                <p>
                    <a href="https://access.redhat.com/articles/yum-cheat-sheet">
                        Yum cheat-sheet
                    </a>
                </p>
            </>
        ),
    },
    {
        section: 'docker',
        question: () => <span>apply the latest content updates</span>,
        answer: () => (
            <>
                <CodeCopy>cd vp</CodeCopy>
                <CodeCopy>cd checkout press</CodeCopy>
                <CodeCopy>git pull -r</CodeCopy>
                A.com -{' '}
                <CodeCopy inline>
                    docker exec -i -t asuriondotcom_wordpress_1 bash
                </CodeCopy>
                <br />
                ServiceBench -{' '}
                <CodeCopy inline>
                    docker exec -i -t servicebench-portal_wordpress_1 bash
                </CodeCopy>
                <br />
                <CodeCopy inline>
                    $APP_DIR/bin/docker/utils/apply-content-changes.sh
                </CodeCopy>
                <CodeCopy>exit</CodeCopy>
                <CodeCopy>cd ..</CodeCopy>
            </>
        ),
    },
    {
        section: 'docker',
        question: () => <span>change / update WordPress version</span>,
        answer: () => (
            <>
                <p>Update version number in:</p>
                <ul>
                    <li>
                        <code>Dockerfile:1</code>
                    </li>
                    <li>
                        <code>package.json:64</code>
                    </li>
                </ul>
                <CodeCopy>yarn local-stop</CodeCopy>
                <CodeCopy>yarn local-build</CodeCopy>
                <CodeCopy>yarn local-start</CodeCopy>
            </>
        ),
    },
]);
