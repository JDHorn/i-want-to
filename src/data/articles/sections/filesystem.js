import React from 'react';
import { Link } from 'react-router-dom';
import { sortArticles } from '../../../components/utilities';
export default sortArticles([
    {
        section: 'filesystem',
        question: () => <span>create a new article</span>,
        answer: () => (
            <>
                <p>
                    Visit <Link to="/article">Create an article</Link>, and
                    enter your code within the appropriate fields
                </p>
                <p>
                    Once the fields are filled in, click the "Copy code" button
                    to copy the new article to the clipboard.
                </p>
            </>
        ),
    },
]);
