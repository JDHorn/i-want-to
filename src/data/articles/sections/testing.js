import React from 'react';
import { sortArticles } from '../../../components/utilities';
export default sortArticles([
    {
        section: 'testing',
        question: () => <span>see the Jest cheat sheet</span>,
        answer: () => (
            <>
                <p>
                    Visit{' '}
                    <a href="https://github.com/sapegin/jest-cheat-sheet/blob/master/Readme.md">
                        https://github.com/sapegin/jest-cheat-sheet/blob/master/Readme.md
                    </a>
                </p>
            </>
        ),
    },
]);
