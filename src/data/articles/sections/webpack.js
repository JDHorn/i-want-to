import CodeCopy from '../../../components/codeCopy';
import React from 'react';
import { sortArticles } from '../../../components/utilities';

export default sortArticles([
    {
        section: 'webpack',
        question: () => <span>compile js & scss in development mode</span>,
        answer: () => (
            <>
                <p>
                    Run this in the root of the project. Compiles both frontend
                    js and Gutenberg blocks with source maps
                </p>
                <CodeCopy>yarn dev</CodeCopy>
            </>
        ),
    },
    {
        section: 'webpack',
        question: () => <span>compile js & scss for production</span>,
        answer: () => (
            <>
                <p>
                    Run this in the root of the project. Compiles both frontend
                    js and Gutenberg blocks minified and with no source maps
                </p>
                <CodeCopy>yarn build</CodeCopy>
            </>
        ),
    },
]);
