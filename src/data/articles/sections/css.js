import React from 'react';
import { sortArticles } from '../../../components/utilities';
import CodeCopy from '../../../components/codeCopy';
export default sortArticles([
    {
        section: 'css',
        question: () => <span>import site variables and mix-ins</span>,
        answer: () => (
            <>
                <p>
                    Needed to refer to variables, etc. within scss files used
                    for components
                </p>
                <CodeCopy>
                    @import '../../../sass/variables-site/variables-site';
                    <br />
                    @import '../../../sass/mixins/mixins-master';
                </CodeCopy>
            </>
        ),
    },
]);
