import React from 'react';
import CodeCopy from '../../../components/codeCopy';
import { sortArticles } from '../../../components/utilities';
import ShowHide from '../../../components/ShowHide';
/* eslint-disable */
export default sortArticles(
    [
        {
            section: 'bookmarklets',
            question: () => <span>1) What is a "bookmarklet"?</span>,
            answer: () => (
                <>
                    <p>
                        An excerpt from{' '}
                        <em>
                            <a href="https://en.wikipedia.org/wiki/Bookmarklet">
                                Wikipedia
                            </a>
                        </em>
                    </p>
                    <p>
                        A bookmarklet is a bookmark stored in a web browser that
                        contains JavaScript commands that add new features to
                        the browser. Bookmarklets are unobtrusive JavaScripts
                        stored as the URL of a bookmark in a web browser or as a
                        hyperlink on a web page. Bookmarklets are usually
                        JavaScript programs. Regardless of whether bookmarklet
                        utilities are stored as bookmarks or hyperlinks, they
                        add one-click functions to a browser or web page. When
                        clicked, a bookmarklet performs one of a wide variety of
                        operations, such as running a search query or extracting
                        data from a table.
                    </p>
                </>
            ),
        },
        {
            section: 'bookmarklets',
            question: () => <span>2) Add a "bookmarklet" to my browser?</span>,
            answer: () => (
                <>
                    <p>Simply drag a link to your bookmark bar.</p>
                </>
            ),
        },
        {
            section: 'bookmarklets',
            question: () => (
                <span>add headings, font sizes, and content to a.com</span>
            ),
            answer: () => (
                <>
                    <p>
                        Drag this link to your bookmark bar:{' '}
                        <a href="javascript:void((function(){!function(e){var t={};function i(n){if(t[n])return t[n].exports;var a=t[n]={i:n,l:!1,exports:{}};return e[n].call(a.exports,a,a.exports,i),a.l=!0,a.exports}i.m=e,i.c=t,i.d=function(e,t,n){i.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:n})},i.r=function(e){'undefined'!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:'Module'}),Object.defineProperty(e,'__esModule',{value:!0})},i.t=function(e,t){if(1&t&&(e=i(e)),8&t)return e;if(4&t&&'object'==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(i.r(n),Object.defineProperty(n,'default',{enumerable:!0,value:e}),2&t&&'string'!=typeof e)for(var a in e)i.d(n,a,function(t){return e[t]}.bind(null,a));return n},i.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return i.d(t,'a',t),t},i.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},i.p='',i(i.s=0)}([function(e,t){function i(e){return function(e){if(Array.isArray(e)){for(var t=0,i=new Array(e.length);t<e.length;t++)i[t]=e[t];return i}}(e)||function(e){if(Symbol.iterator in Object(e)||'[object Arguments]'===Object.prototype.toString.call(e))return Array.from(e)}(e)||function(){throw new TypeError('Invalid attempt to spread non-iterable instance')}()}function n(){var e=['Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Quisque cursus, metus vitae pharetra auctor, sem massa mattis sem, at interdum magna augue eget diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi lacinia molestie dui. Praesent blandit dolor. Sed non quam. In vel mi sit amet augue congue elementum. Morbi in ipsum sit amet pede facilisis laoreet. Donec lacus nunc, viverra nec, blandit vel, egestas et, augue. Vestibulum tincidunt malesuada tellus. Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus.','Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante. Nulla quam. Aenean laoreet. Vestibulum nisi lectus, commodo ac, facilisis ac, ultricies eu, pede. Ut orci risus, accumsan porttitor, cursus quis, aliquet eget, justo. Sed pretium blandit orci. Ut eu diam at pede suscipit sodales. Aenean lectus elit, fermentum non, convallis id, sagittis at, neque.','Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante. Sed lacinia, urna non tincidunt mattis, tortor neque adipiscing diam, a cursus ipsum ante quis turpis. Nulla facilisi. Ut fringilla. Suspendisse potenti. Nunc feugiat mi a tellus consequat imperdiet. Vestibulum sapien. Proin quam. Etiam ultrices. Suspendisse in justo eu magna luctus suscipit. Sed lectus. Integer euismod lacus luctus magna.','Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh. Quisque volutpat condimentum velit.'],t=document.createElement('div');t.className='generated-content';for(var i,n=0;n++<6;){var a=document.createElement('h'.concat(n));a.innerText='H'.concat(n);var u=document.createElement('p');u.innerText=e[(i=e.length,parseInt(Math.random()*i))],t.appendChild(a),t.appendChild(u)}return t}var a=document.getElementsByClassName('entry-content')[0],u=document.getElementsByClassName('single-content')[0],r=document.getElementsByClassName('single-heading')[0];r&&r.appendChild(n()),a&&a.appendChild(n()),u&&u.appendChild(n());for(var s=0;s++<6;){i(document.querySelectorAll('H'.concat(s))).forEach(function(e){var t=window.getComputedStyle(e)['font-size'],n=window.getComputedStyle(e)['font-family'].split(',')[0],a=document.createElement('span'),u=e.classList.length?'.'.concat(i(e.classList).join('.')):'';a.style.fontSize='1rem',a.style.fontFamily='Fira Code, monospace',a.innerText=' ('.concat(e.tagName).concat(u,': ').concat(t,' ').concat(n,')'),e.appendChild(a)})}}]);})());">
                            Headings, etc.
                        </a>
                    </p>
                    <p>
                        Or paste the code below into an existing bookmark
                        <br />
                        <ShowHide>
                            <CodeCopy>
                                {
                                    "javascript:void((function(){!function(e){var t={};function i(n){if(t[n])return t[n].exports;var a=t[n]={i:n,l:!1,exports:{}};return e[n].call(a.exports,a,a.exports,i),a.l=!0,a.exports}i.m=e,i.c=t,i.d=function(e,t,n){i.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:n})},i.r=function(e){'undefined'!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:'Module'}),Object.defineProperty(e,'__esModule',{value:!0})},i.t=function(e,t){if(1&t&&(e=i(e)),8&t)return e;if(4&t&&'object'==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(i.r(n),Object.defineProperty(n,'default',{enumerable:!0,value:e}),2&t&&'string'!=typeof e)for(var a in e)i.d(n,a,function(t){return e[t]}.bind(null,a));return n},i.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return i.d(t,'a',t),t},i.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},i.p='',i(i.s=0)}([function(e,t){function i(e){return function(e){if(Array.isArray(e)){for(var t=0,i=new Array(e.length);t<e.length;t++)i[t]=e[t];return i}}(e)||function(e){if(Symbol.iterator in Object(e)||'[object Arguments]'===Object.prototype.toString.call(e))return Array.from(e)}(e)||function(){throw new TypeError('Invalid attempt to spread non-iterable instance')}()}function n(){var e=['Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Quisque cursus, metus vitae pharetra auctor, sem massa mattis sem, at interdum magna augue eget diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi lacinia molestie dui. Praesent blandit dolor. Sed non quam. In vel mi sit amet augue congue elementum. Morbi in ipsum sit amet pede facilisis laoreet. Donec lacus nunc, viverra nec, blandit vel, egestas et, augue. Vestibulum tincidunt malesuada tellus. Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus.','Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante. Nulla quam. Aenean laoreet. Vestibulum nisi lectus, commodo ac, facilisis ac, ultricies eu, pede. Ut orci risus, accumsan porttitor, cursus quis, aliquet eget, justo. Sed pretium blandit orci. Ut eu diam at pede suscipit sodales. Aenean lectus elit, fermentum non, convallis id, sagittis at, neque.','Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante. Sed lacinia, urna non tincidunt mattis, tortor neque adipiscing diam, a cursus ipsum ante quis turpis. Nulla facilisi. Ut fringilla. Suspendisse potenti. Nunc feugiat mi a tellus consequat imperdiet. Vestibulum sapien. Proin quam. Etiam ultrices. Suspendisse in justo eu magna luctus suscipit. Sed lectus. Integer euismod lacus luctus magna.','Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh. Quisque volutpat condimentum velit.'],t=document.createElement('div');t.className='generated-content';for(var i,n=0;n++<6;){var a=document.createElement('h'.concat(n));a.innerText='H'.concat(n);var u=document.createElement('p');u.innerText=e[(i=e.length,parseInt(Math.random()*i))],t.appendChild(a),t.appendChild(u)}return t}var a=document.getElementsByClassName('entry-content')[0],u=document.getElementsByClassName('single-content')[0],r=document.getElementsByClassName('single-heading')[0];r&&r.appendChild(n()),a&&a.appendChild(n()),u&&u.appendChild(n());for(var s=0;s++<6;){i(document.querySelectorAll('H'.concat(s))).forEach(function(e){var t=window.getComputedStyle(e)['font-size'],n=window.getComputedStyle(e)['font-family'].split(',')[0],a=document.createElement('span'),u=e.classList.length?'.'.concat(i(e.classList).join('.')):'';a.style.fontSize='1rem',a.style.fontFamily='Fira Code, monospace',a.innerText=' ('.concat(e.tagName).concat(u,': ').concat(t,' ').concat(n,')'),e.appendChild(a)})}}]);})());"
                                }
                            </CodeCopy>
                        </ShowHide>
                    </p>
                </>
            ),
        },
        {
            section: 'bookmarklets',
            question: () => (
                <span>
                    open the same page in different environments (press,
                    sandbox, UAT, etc)
                </span>
            ),
            answer: () => (
                <>
                    <p>
                        Drag this link to your bookmark bar:{' '}
                        <a href="javascript:void((function(){!function(e){var t={};function n(r){if(t[r])return t[r].exports;var a=t[r]={i:r,l:!1,exports:{}};return e[r].call(a.exports,a,a.exports,n),a.l=!0,a.exports}n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){'undefined'!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:'Module'}),Object.defineProperty(e,'__esModule',{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&'object'==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,'default',{enumerable:!0,value:e}),2&t&&'string'!=typeof e)for(var a in e)n.d(r,a,function(t){return e[t]}.bind(null,a));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,'a',t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p='',n(n.s=0)}([function(e,t,n){'use strict';n.r(t);var r={local:{friendly:'Local',url:'http://localhost'},press:{friendly:'Press',url:'https://uat-press.use1.sqa.aws.asurion.net'},uat:{friendly:'UAT',url:'https://uat-adc.asurion.com'},prod:{friendly:'Production',url:'https://www.asurion.com'},ghostlabs:{friendly:'Ghostlabs',url:'http://192.168.1.85:8005'},sandbox:{friendly:'Sandbox',url:'https://press-sandbox.use1.sqa.aws.asurion.net'},classic:{friendly:'Classic',url:'https://asupub-prd.use1.prd.aws.asurion.net'}};function a(e,t){if(null==e)return{};var n,r,a=function(e,t){if(null==e)return{};var n,r,a={},o=Object.keys(e);for(r=0;r<o.length;r++)n=o[r],t.indexOf(n)>=0||(a[n]=e[n]);return a}(e,t);if(Object.getOwnPropertySymbols){var o=Object.getOwnPropertySymbols(e);for(r=0;r<o.length;r++)n=o[r],t.indexOf(n)>=0||Object.prototype.propertyIsEnumerable.call(e,n)&&(a[n]=e[n])}return a}var o=location.pathname,l=c({tagName:'div',id:'external-links'}),u=c({tagName:'div',id:'external-links-overlay'}),i=c({tagName:'ul'}),s=c({tagName:'link',href:'https://s3-us-west-2.amazonaws.com/s.cdpn.io/61776/links_1.css',type:'text/css',rel:'stylesheet',id:'external-links-styles'});function c(e){var t=e.tagName,n=a(e,['tagName']),r=document.createElement(t);return Object.keys(n).forEach(function(e){return r[e]=n[e]}),r}u.addEventListener('click',function(){[l,u,s].forEach(function(e){e.parentElement.removeChild(e)})}),Object.keys(r).forEach(function(e){return function(e){if(!e)return;var t=c({tagName:'li'});t.appendChild(e),i.appendChild(t)}(function(e){if(location.origin===r[e].url)return;return c({tagName:'a',target:'_blank',href:r[e].url+o,innerText:r[e].friendly})}(e))}),l.appendChild(i),document.head.appendChild(s),document.body.appendChild(u),document.body.appendChild(l)}]);})());">
                            ENVs
                        </a>
                    </p>
                    <p>
                        Or paste the code below into an existing bookmark
                        <br />
                        <ShowHide>
                            <CodeCopy>
                                {
                                    "javascript:void((function(){!function(e){var t={};function n(r){if(t[r])return t[r].exports;var a=t[r]={i:r,l:!1,exports:{}};return e[r].call(a.exports,a,a.exports,n),a.l=!0,a.exports}n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){'undefined'!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:'Module'}),Object.defineProperty(e,'__esModule',{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&'object'==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,'default',{enumerable:!0,value:e}),2&t&&'string'!=typeof e)for(var a in e)n.d(r,a,function(t){return e[t]}.bind(null,a));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,'a',t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p='',n(n.s=0)}([function(e,t,n){'use strict';n.r(t);var r={local:{friendly:'Local',url:'http://localhost'},press:{friendly:'Press',url:'https://uat-press.use1.sqa.aws.asurion.net'},uat:{friendly:'UAT',url:'https://uat-adc.asurion.com'},prod:{friendly:'Production',url:'https://www.asurion.com'},ghostlabs:{friendly:'Ghostlabs',url:'http://192.168.1.85:8005'},sandbox:{friendly:'Sandbox',url:'https://press-sandbox.use1.sqa.aws.asurion.net'},classic:{friendly:'Classic',url:'https://asupub-prd.use1.prd.aws.asurion.net'}};function a(e,t){if(null==e)return{};var n,r,a=function(e,t){if(null==e)return{};var n,r,a={},o=Object.keys(e);for(r=0;r<o.length;r++)n=o[r],t.indexOf(n)>=0||(a[n]=e[n]);return a}(e,t);if(Object.getOwnPropertySymbols){var o=Object.getOwnPropertySymbols(e);for(r=0;r<o.length;r++)n=o[r],t.indexOf(n)>=0||Object.prototype.propertyIsEnumerable.call(e,n)&&(a[n]=e[n])}return a}var o=location.pathname,l=c({tagName:'div',id:'external-links'}),u=c({tagName:'div',id:'external-links-overlay'}),i=c({tagName:'ul'}),s=c({tagName:'link',href:'https://s3-us-west-2.amazonaws.com/s.cdpn.io/61776/links_1.css',type:'text/css',rel:'stylesheet',id:'external-links-styles'});function c(e){var t=e.tagName,n=a(e,['tagName']),r=document.createElement(t);return Object.keys(n).forEach(function(e){return r[e]=n[e]}),r}u.addEventListener('click',function(){[l,u,s].forEach(function(e){e.parentElement.removeChild(e)})}),Object.keys(r).forEach(function(e){return function(e){if(!e)return;var t=c({tagName:'li'});t.appendChild(e),i.appendChild(t)}(function(e){if(location.origin===r[e].url)return;return c({tagName:'a',target:'_blank',href:r[e].url+o,innerText:r[e].friendly})}(e))}),l.appendChild(i),document.head.appendChild(s),document.body.appendChild(u),document.body.appendChild(l)}]);})());"
                                }
                            </CodeCopy>
                        </ShowHide>
                    </p>
                </>
            ),
        },
        {
            section: 'bookmarklets',
            question: () => <span>see when an environment was deployed</span>,
            answer: () => (
                <>
                    <p>
                        Visit the{' '}
                        <a href="https://www.asurion.com/apphealthcheck.html">
                            apphealthcheck.html page
                        </a>
                        , then use this bookmarklet to format the date/time of
                        the last deployment.
                    </p>
                    <p>
                        Drag this link to your bookmark bar:{' '}
                        <a href="javascript:void((function(){!function(e){var t={};function n(r){if(t[r])return t[r].exports;var u=t[r]={i:r,l:!1,exports:{}};return e[r].call(u.exports,u,u.exports,n),u.l=!0,u.exports}n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){'undefined'!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:'Module'}),Object.defineProperty(e,'__esModule',{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&'object'==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,'default',{enumerable:!0,value:e}),2&t&&'string'!=typeof e)for(var u in e)n.d(r,u,function(t){return e[t]}.bind(null,u));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,'a',t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p='',n(n.s=0)}([function(e,t){var n=document.getElementsByTagName('h1')[0],r=n.innerText.replace(/[^\d+]/g,''),u=r.substring(0,4),o=r.substring(4,6)-1,i=r.substring(6,8),f=r.substring(8,10),l=r.substring(10,12),a=r.substring(12,14);n.innerHTML+='<br>'+new Date(Date.UTC(u,o,i,f,l,a))}]);})());">
                            Deploy Date & Time
                        </a>
                    </p>
                    <p>
                        Or paste the code below into an existing bookmark
                        <br />
                        <ShowHide>
                            <CodeCopy>
                                {
                                    "javascript:void((function(){!function(e){var t={};function n(r){if(t[r])return t[r].exports;var u=t[r]={i:r,l:!1,exports:{}};return e[r].call(u.exports,u,u.exports,n),u.l=!0,u.exports}n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){'undefined'!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:'Module'}),Object.defineProperty(e,'__esModule',{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&'object'==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,'default',{enumerable:!0,value:e}),2&t&&'string'!=typeof e)for(var u in e)n.d(r,u,function(t){return e[t]}.bind(null,u));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,'a',t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p='',n(n.s=0)}([function(e,t){var n=document.getElementsByTagName('h1')[0],r=n.innerText.replace(/[^\\d+]/g,''),u=r.substring(0,4),o=r.substring(4,6)-1,i=r.substring(6,8),f=r.substring(8,10),l=r.substring(10,12),a=r.substring(12,14);n.innerHTML+='<br>'+new Date(Date.UTC(u,o,i,f,l,a))}]);})());"
                                }
                            </CodeCopy>
                        </ShowHide>
                    </p>
                </>
            ),
        },
        {
            section: 'bookmarklets',
            question: () => <span>display the asset catalog</span>,
            answer: () => (
                <>
                    <p>
                        Jim Updike's script. Use on{' '}
                        <a href="https://rtm-crmjs-sq-vip1.use1.sqa.aws.asurion.net/">
                            https://rtm-crmjs-sq-vip1.use1.sqa.aws.asurion.net/
                        </a>
                    </p>
                    <p>
                        Drag this link to your bookmark bar:{' '}
                        <a href="javascript:void((function(){!function(e){var t={};function n(r){if(t[r])return t[r].exports;var o=t[r]={i:r,l:!1,exports:{}};return e[r].call(o.exports,o,o.exports,n),o.l=!0,o.exports}n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){'undefined'!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:'Module'}),Object.defineProperty(e,'__esModule',{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&'object'==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,'default',{enumerable:!0,value:e}),2&t&&'string'!=typeof e)for(var o in e)n.d(r,o,function(t){return e[t]}.bind(null,o));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,'a',t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p='',n(n.s=0)}([function(e,t){!function(){document.body.innerHTML='<div id='app'></div>';var e=document.createElement('script');e.onload=function(){console.log('onload was called!')},e.src='https://enterprise-bitbucket.asurion.com/projects/WMS/repos/asset-catalog-tester/raw/dist/public/main.js?at=refs%2Fheads%2Fmaster',document.body.appendChild(e)}()}]);})());">
                            Asset catalog
                        </a>
                    </p>
                    <p>
                        Or paste the code below into an existing bookmark
                        <br />
                        <ShowHide>
                            <CodeCopy>
                                {
                                    "javascript:void((function(){!function(e){var t={};function n(r){if(t[r])return t[r].exports;var o=t[r]={i:r,l:!1,exports:{}};return e[r].call(o.exports,o,o.exports,n),o.l=!0,o.exports}n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){'undefined'!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:'Module'}),Object.defineProperty(e,'__esModule',{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&'object'==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,'default',{enumerable:!0,value:e}),2&t&&'string'!=typeof e)for(var o in e)n.d(r,o,function(t){return e[t]}.bind(null,o));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,'a',t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p='',n(n.s=0)}([function(e,t){!function(){document.body.innerHTML='<div id='app'></div>';var e=document.createElement('script');e.onload=function(){console.log('onload was called!')},e.src='https://enterprise-bitbucket.asurion.com/projects/WMS/repos/asset-catalog-tester/raw/dist/public/main.js?at=refs%2Fheads%2Fmaster',document.body.appendChild(e)}()}]);})());"
                                }
                            </CodeCopy>
                        </ShowHide>
                    </p>
                </>
            ),
        },
    ],
    'question'
);
