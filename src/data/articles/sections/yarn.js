import CodeCopy from '../../../components/codeCopy';
import React from 'react';
import { sortArticles } from '../../../components/utilities';

export default sortArticles([
    {
        section: 'yarn',
        question: () => <span>install dependencies</span>,
        answer: () => (
            <>
                <p>Installs node_modules</p>
                <CodeCopy>yarn</CodeCopy>
            </>
        ),
    },
    {
        section: 'yarn',
        question: () => <span>add a new dependency</span>,
        answer: () => (
            <>
                <p>
                    Installs a new dependency to node_modules (and adds it to
                    dependencies in the package.json file)
                </p>
                <CodeCopy>yarn add &lt;module_name&gt;</CodeCopy>
            </>
        ),
    },
    {
        section: 'yarn',
        question: () => <span>add a new dev dependency</span>,
        answer: () => (
            <>
                <p>
                    Installs a new dependency to node_modules (and adds it to{' '}
                    <code>devDependencies</code> in the package.json file)
                </p>
                <CodeCopy>yarn add &lt;module_name&gt; --dev</CodeCopy>
            </>
        ),
    },
    {
        section: 'yarn',
        question: () => <span>install / update yarn</span>,
        seldom: true,
        answer: () => (
            <CodeCopy>
                curl -o- -L https://yarnpkg.com/install.sh | bash
            </CodeCopy>
        ),
    },
    {
        section: 'yarn',
        question: () => <span>run a script</span>,
        answer: () => (
            <>
                <CodeCopy>yarn &lt;script-name></CodeCopy>
                <p>
                    Note that the <code>run</code> keyword isn't necessary like
                    it is with <code>npm</code>
                </p>
            </>
        ),
    },
]);
