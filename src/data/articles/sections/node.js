import React from 'react';
import { sortArticles } from '../../../components/utilities';
import CodeCopy from '../../../components/codeCopy';
export default sortArticles([
    {
        section: 'node',
        question: () => <span>use a specific version of node</span>,
        answer: () => (
            <>
                <CodeCopy>nvm use &lt;version></CodeCopy>
            </>
        ),
    },
    {
        section: 'node',
        question: () => <span>install node version manager (nvm)</span>,
        answer: () => (
            <>
                <CodeCopy>
                    curl -o-
                    https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh
                    | bash
                </CodeCopy>
            </>
        ),
    },
    {
        section: 'node',
        question: () => <span>debug node scripts</span>,
        answer: () => (
            <>
                <CodeCopy>node --inspect &lt;filename></CodeCopy>
                <CodeCopy>node --inspect-brk &lt;filename></CodeCopy>
                <p>
                    Visit about:inspect in chrome and choose the remote target
                    that points to your node instance
                </p>
            </>
        ),
    },
]);
