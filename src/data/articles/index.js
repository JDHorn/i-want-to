import bookmarklets from './sections/bookmarklets';
import css from './sections/css';
import docker from './sections/docker';
import filesystem from './sections/filesystem';
import git from './sections/git';
import gutenberg from './sections/gutenberg';
import misc from './sections/misc';
import node from './sections/node';
import testing from './sections/testing';
import webpack from './sections/webpack';
import terminal from './sections/terminal';
import yarn from './sections/yarn';

export default [].concat(
    bookmarklets,
    css,
    docker,
    filesystem,
    git,
    gutenberg,
    misc,
    node,
    testing,
    webpack,
    terminal,
    yarn
);
