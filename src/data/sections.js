import { sort } from '../components/utilities';

export default sort(
    [
        { section: 'bookmarklets', friendlyName: 'Bookmarklets' },
        { section: 'css', friendlyName: 'CSS' },
        { section: 'docker', friendlyName: 'Docker' },
        // { section: 'filesystem', friendlyName: 'File system' },
        { section: 'git', friendlyName: 'Git' },
        { section: 'gutenberg', friendlyName: 'Gutenberg' },
        { section: 'misc', friendlyName: 'Misc.' },
        { section: 'node', friendlyName: 'Node' },
        { section: 'terminal', friendlyName: 'Terminal' },
        { section: 'testing', friendlyName: 'Testing' },
        { section: 'webpack', friendlyName: 'Webpack' },
        { section: 'yarn', friendlyName: 'Yarn' },
    ],
    'section'
);
